#### **NATUAL LANGUAGE-PROCESSING PROGRAM**

        This program processes natural language and executes the following operations:
            - tokenizing (for words and sentences)
            - deleting stopwords
            - lemmatizing
            - stemming
            - tagging
            - classification of tokens (frequency distribution)
            - lexical analysis (definitions, synonyms, antonyms)
            
        To operate the program simply input your text file into the 'text_files folder' and run the code.
    To executes the aforementioned operations input relevant valuse when reuired:
            "1" - for tokenizing, deleting stopwords
            "2" - for lemmatizing, stemming and tagging
            "3" - for classification of tokens; frequency distribution
            "4" - for lexical analysis: searching for definitions, synonyms, antonyms
           
        The program is initialized through the terminal. Input the "--h' key to get a prompt
    with further instructions for the program.