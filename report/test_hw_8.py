import nltk
from nltk.corpus import stopwords 
from nltk.tokenize import word_tokenize 

from tensorflow.keras.layers import SimpleRNN
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
import numpy as np
import itertools

import api
import backend

def convert_str (string):
    li = list(string.split(" "))
    return li



# INPUT_FILE = "lesson8/text_fragment.txt"
# INPUT_FILE = "lesson8/alice_in_wonderland.txt"
INPUT_FILE = "lesson8/alice_in_wonderland_short.txt"
# INPUT_FILE = "lesson8/crime_and_punishment.txt"


# extract the input as a stream of words
print("Extracting text from input...")
fin = open(INPUT_FILE, 'rb')
lines = []
for line in fin:
    line = line.strip().lower()
    line = line.decode("ascii", "ignore")
    if len(line) == 0:
        continue
    lines.append(line)
fin.close()
raw_data = " ".join(lines)

# print(raw_data)

tokens = backend.TokensStopwords.w_tokenizer(raw_data)

# print(type(tokens))

# print(len(tokens))

# stop_word = backend.TokensStopwords.stats_qualifier(raw_data)

# print(stop_word)

# lemma_text = backend.SemanticProcessing.lemmatizing(raw_data)

# print(lemma_text)

# creating lookup tables
# Here chars is the number of features in our character "vocabulary"
nb_chars = len(tokens)
char2index = dict((c, i) for i, c in enumerate(tokens))
index2char = dict((i, c) for i, c in enumerate(tokens))

print(type(tokens))

# create inputs and labels from the text. We do this by stepping
# through the text ${step} character at a time, and extracting a 
# sequence of size ${seqlen} and the next output char. For example,
# assuming an input text "The sky was falling", we would get the 
# following sequence of input_chars and label_chars (first 5 only)
#   The sky wa -> s
#   he sky was ->  
#   e sky was  -> f
#    sky was f -> a
#   sky was fa -> l
print("Creating input and label text...")
SEQLEN = 5
STEP = 1


# index_test = 
# print(index_test) len(index2char) - SEQLEN


input_chars = []
label_chars = []
for i in range(0, len(tokens) - SEQLEN, STEP):
    input_chars.append(list(itertools.islice(tokens, i, i + SEQLEN)))
    label_chars.append(tokens[i + SEQLEN])

# index_test = index2char[:7]
# print(index_test)

# print(input_chars)
# print(label_chars)


# vectorize the input and label chars
# Each row of the input is represented by seqlen characters, each 
# represented as a 1-hot encoding of size len(char). There are 
# len(input_chars) such rows, so shape(X) is (len(input_chars),
# seqlen, nb_chars).
# Each row of output is a single character, also represented as a
# dense encoding of size len(char). Hence shape(y) is (len(input_chars),
# nb_chars).
print("Vectorizing input and label text...")
X = np.zeros((len(input_chars), SEQLEN, nb_chars), dtype=np.bool)
y = np.zeros((len(input_chars), nb_chars), dtype=np.bool)

# print(X)
# print(y)


for i, input_char in enumerate(input_chars):
    for j, ch in enumerate(input_char):
        X[i, j, char2index[ch]] = 1
    y[i, char2index[label_chars[i]]] = 1



# Build the model. We use a single RNN with a fully connected layer
# to compute the most likely predicted output char
HIDDEN_SIZE = 128
BATCH_SIZE = 128
NUM_ITERATIONS = 50
NUM_EPOCHS_PER_ITERATION = 1
NUM_PREDS_PER_EPOCH = 100

model = Sequential()
model.add(SimpleRNN(HIDDEN_SIZE,
                    return_sequences=False,
                    input_shape=(SEQLEN, nb_chars),
                    unroll=True))
model.add(Dense(nb_chars))
model.add(Activation("softmax"))

model.compile(loss="categorical_crossentropy", optimizer="rmsprop")

# We train the model in batches and test output generated at each step
for iteration in range(NUM_ITERATIONS):
    print("=" * 50)
    print("Iteration #: %d" % (iteration))
    model.fit(X, y, batch_size=BATCH_SIZE, epochs=NUM_EPOCHS_PER_ITERATION)
    
    # testing model
    # randomly choose a row from input_chars, then use it to 
    # generate text from model for next 100 chars
    test_idx = np.random.randint(len(input_chars))
    test_chars = input_chars[test_idx]
    print("Generating from seed: %s" % (test_chars))
    print(test_chars, end="")
    for i in range(NUM_PREDS_PER_EPOCH):
        Xtest = np.zeros((1, SEQLEN, nb_chars))
        for i, ch in enumerate(test_chars):
            Xtest[0, i, char2index[ch]] = 1
        pred = model.predict(Xtest, verbose=0)[0]
        ypred = index2char[np.argmax(pred)]
        print(ypred, end=" ")
        ypred = convert_str(ypred)
        # move forward with test_chars + ypred
        test_chars = test_chars[1:] + ypred
    print()